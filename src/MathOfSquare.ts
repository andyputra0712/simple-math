export class MathOfSquare {
    getSquareRoot(num: number): number {
      return Math.sqrt(num);
    }
  
    getSquare(num: number): number {
      return num * num;
    }
  }
  