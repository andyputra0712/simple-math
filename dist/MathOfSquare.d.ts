export declare class MathOfSquare {
    getSquareRoot(num: number): number;
    getSquare(num: number): number;
}
