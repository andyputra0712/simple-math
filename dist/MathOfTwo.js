export class MathOfTwo {
    double(num) {
        return num * 2;
    }
    half(num) {
        return num / 2;
    }
}
