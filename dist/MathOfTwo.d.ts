export declare class MathOfTwo {
    double(num: number): number;
    half(num: number): number;
}
