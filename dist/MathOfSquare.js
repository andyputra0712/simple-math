export class MathOfSquare {
    getSquareRoot(num) {
        return Math.sqrt(num);
    }
    getSquare(num) {
        return num * num;
    }
}
